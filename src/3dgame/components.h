#pragma once

namespace Component
{


struct Position
{
    float x = 0.0f;
    float y = 0.0f;
    float angle = 0.0f;
};

struct Velocity
{
    float dx = 0.0f;
    float dy = 0.0f;
    float dangle = 0.0f;
};

struct MoveStepwise
{
    float dx = 0.0f;
    float dy = 0.0f;
    float dangle = 0.0f;

    void reset() { *this = MoveStepwise(); }
};

struct RemoveFlag
{
    bool remove = false;
};

struct Player
{

};

struct Entity
{

};


}
