#include "geometry.h"


std::vector<Geometry> Geometry::make(const sf::Vector2f &pos, const sf::Vector2f &size, MaterialId materialId)
{
    std::vector<Geometry> geos(4);
//    geos.emplace_back(pos,                      sf::Vector2f(pos.x + size.x, pos.y), materialId); // top
//    geos.emplace_back(size,                     sf::Vector2f(pos.x, pos.y + size.y), materialId); // bottom
//    geos.emplace_back(pos,                      sf::Vector2f(pos.x, pos.y + size.y), materialId); // left
//    geos.emplace_back(size,      sf::Vector2f(), materialId); // right

    sf::Vector2f pLeftUp(pos);
    sf::Vector2f pLeftDown(pos.x, pos.y + size.y);
    sf::Vector2f pRightDown(pos + size);
    sf::Vector2f pRightUp(pos.x + size.x, pos.y);

    geos.emplace_back(pLeftUp, pLeftDown, materialId);  // left
    geos.emplace_back(pLeftDown, pRightDown, materialId);  // down
    geos.emplace_back(pRightDown, pRightUp, materialId);  // right
    geos.emplace_back(pRightUp, pLeftUp, materialId);  // up

    return geos;
}

Geometry::Geometry(const sf::Vector2f &start, const sf::Vector2f &end, MaterialId materialId)
    : m_start(start),
      m_end(end),
      m_materialId(materialId)
{}

sf::Vector2f Geometry::start() const
{
    return m_start;
}

sf::Vector2f Geometry::end() const
{
    return m_end;
}

Geometry::MaterialId Geometry::materialId() const
{
    return m_materialId;
}

bool Geometry::isSolid() const
{
    return m_isSolid;
}

void Geometry::setIsSolid(bool isSolid)
{
    m_isSolid = isSolid;
}

bool Geometry::isTransparent() const
{
    return m_isTransparent;
}

void Geometry::setIsTransparent(bool isTransparent)
{
    m_isTransparent = isTransparent;
}
