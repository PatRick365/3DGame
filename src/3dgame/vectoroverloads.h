#pragma once

#include <SFML/System/Vector2.hpp>

//template<typename T>
//sf::Vector2<T> operator /(const sf::Vector2<T> &v, T m)
//{
//    return sf::Vector2<T>(v.x / m, v.y / m);
//}

//template<typename T>
//sf::Vector2<T> operator *(const sf::Vector2<T> &v, float m)
//{
//    return sf::Vector2<T>(v.x * m, v.y * m);
//}

//template<typename T>
//sf::Vector2<T> operator -(const sf::Vector2<T> &v, float m)
//{
//    return sf::Vector2<T>(v.x - m, v.y - m);
//}

//template<typename T>
//sf::Vector2<T> operator +(const sf::Vector2<T> &v, float m)
//{
//    return sf::Vector2<T>(v.x + m, v.y + m);
//}


sf::Vector2f operator /(const sf::Vector2f &v, float m)
{
    return sf::Vector2f(v.x / m, v.y / m);
}

sf::Vector2f operator *(const sf::Vector2f &v, float m)
{
    return sf::Vector2f(v.x * m, v.y * m);
}

sf::Vector2f operator -(const sf::Vector2f &v, float m)
{
    return sf::Vector2f(v.x - m, v.y - m);
}

sf::Vector2f operator +(const sf::Vector2f &v, float m)
{
    return sf::Vector2f(v.x + m, v.y + m);
}
