#include "level.h"

#include "../extern/nlohmann/json.hpp"

#include <stdexcept>
#include <cassert>
#include <fstream>
#include <iostream>
#include <filesystem>


Level::LoadResult Level::loadFromDisk(const std::string &folderName)
{
    m_wallMaterials.clear();

    std::string folderLevel("levels/" + folderName + "/");
    std::string fileJson(folderLevel + "level.json");
    std::string folderAssetsWallTextures(folderLevel + "assets/wall_textures/");

    using namespace nlohmann;

    std::cout << "loading file: " << fileJson << "\n";
    std::cout << "absolute path: " << std::filesystem::absolute(fileJson) << "\n";

    if (!std::filesystem::exists(fileJson))
    {
        return LoadResult::makeError("file " + fileJson + " does not exist");
    }

    std::ifstream ifs(fileJson);

    try
    {
        json root = json::parse(ifs);

        auto vRoot = root.at(0);
        auto vLevel = vRoot.at("level");

        {
            auto vPlayer = vLevel.at("player");
            m_playerStartPos.x = vPlayer.at("x");
            m_playerStartPos.y = vPlayer.at("y");
            m_playerStartAngle = vPlayer.at("angle");
        }

        {
            auto vColorFloor = vLevel.at("color_floor");
            m_colorFloor = sf::Color(vColorFloor[0], vColorFloor[1], vColorFloor[2]);
        }
        {
            auto vColorCeiling = vLevel.at("color_ceiling");
            m_colorCeiling = sf::Color(vColorCeiling[0], vColorCeiling[1], vColorCeiling[2]);
        }

        {
            auto vGeometries = vLevel.at("geometries");
            for (const auto &vGeo : vGeometries)
            {
                auto vStart = vGeo.at("start");
                auto vEnd = vGeo.at("end");
                auto vTexture = vGeo.at("texture");

                Geometry::MaterialId materialId;
                if (vGeo.contains("taint"))
                {
                    auto vTaint = vGeo.at("taint");
                    sf::Color taint(vTaint[0], vTaint[1], vTaint[2]);
                    materialId = addMaterial(folderAssetsWallTextures + std::string(vTexture), taint);
                }
                else
                {
                    materialId = addMaterial(folderAssetsWallTextures + std::string(vTexture));
                }

                addGeometry(Geometry(sf::Vector2f(vStart[0], vStart[1]), sf::Vector2f(vEnd[0], vEnd[1]), materialId));
            }
        }
    }
    catch (json::exception jsonException)
    {
        return LoadResult::makeError(std::string("json error: ") + jsonException.what());
    }

    for (auto &geo : m_geometries)
    {
        geo.setIsTransparent(wallTexture(geo.materialId()).isTransparent());
    }

    std::cout << "loaded " << m_geometries.size() << " geometries\n";
    std::cout << "loaded " << m_wallMaterials.size() << " wall materials\n";

    return LoadResult::makeOk();
}


void Level::loadIt()
{
    m_playerStartPos = {1, 1};
    m_playerStartAngle = 0;

    auto idW = addMaterial("levels/test_level_1/assets/wall_textures/w.png");
    auto idN = addMaterial("levels/test_level_1/assets/wall_textures/n.png");
    auto idE = addMaterial("levels/test_level_1/assets/wall_textures/e.png");
    auto idS = addMaterial("levels/test_level_1/assets/wall_textures/s.png");
    auto idBricks = addMaterial("levels/test_level_1/assets/wall_textures/bricks.png");
    auto idQuartz = addMaterial("levels/test_level_1/assets/wall_textures/chiseled_quartz_block_top.png");
    auto idCobble = addMaterial("levels/test_level_1/assets/wall_textures/cobblestone.png");

    auto idGlassTintetRed = addMaterial("levels/test_level_1/assets/wall_textures/tinted_glass.png", sf::Color(255, 0, 0));
    auto idGlassTintetYellow = addMaterial("levels/test_level_1/assets/wall_textures/tinted_glass.png", sf::Color(255, 255, 0));
    auto idGlassTintetGreen = addMaterial("levels/test_level_1/assets/wall_textures/tinted_glass.png", sf::Color(0, 255, 0));
    auto idGlassTintetBlue = addMaterial("levels/test_level_1/assets/wall_textures/tinted_glass.png", sf::Color(0, 0, 255));

    constexpr int width = 10;
    constexpr int height = 10;

    sf::Vector2f pLeftUp(0, 0);
    sf::Vector2f pLeftDown(0, height);
    sf::Vector2f pRightDown(width, height);
    sf::Vector2f pRightUp(width, 0);


    // top
    addGeometry(Geometry(pLeftUp, pRightUp, idCobble));
    // bottom
    addGeometry(Geometry(pRightDown, pLeftDown, idCobble));
    // left
    addGeometry(Geometry(pLeftDown, pLeftUp, idCobble));
    // right
    addGeometry(Geometry(pRightUp, pRightDown, idCobble));

    //addGeometries(Geometry::make(sf::Vector2f(5, 5), sf::Vector2f(1, 1), idBricks));

    sf::Vector2f pos1(5, 5);
    sf::Vector2f pos2(5, 6);
    sf::Vector2f pos3(6, 6);
    addGeometry(Geometry(pos1, pos2, idGlassTintetRed));
    addGeometry(Geometry(pos2, pos3, idGlassTintetGreen));
    addGeometry(Geometry(pos3, pos1, idGlassTintetBlue));

    addGeometry(Geometry(sf::Vector2f(7, 7), sf::Vector2f(6, 7), idGlassTintetBlue));

    m_colorFloor = sf::Color::Green;
    m_colorCeiling = sf::Color::Blue;

    for (auto &geo : m_geometries)
    {
        geo.setIsTransparent(wallTexture(geo.materialId()).isTransparent());
    }
}

const sf::Color &Level::colorFloor() const
{
    return m_colorFloor;
}

const sf::Color &Level::colorCeiling() const
{
    return m_colorCeiling;
}

sf::Vector2f Level::playerStartPos() const
{
    return m_playerStartPos;
}

float Level::playerStartAngle() const
{
    return m_playerStartAngle;
}

const std::vector<Material> &Level::wallTextures() const
{
    return m_wallMaterials;
}

Material &Level::wallTexture(Geometry::MaterialId id)
{
    assert(static_cast<size_t>(id) < m_wallMaterials.size());

    return m_wallMaterials[static_cast<size_t>(id)];
}

Geometry::MaterialId Level::addMaterial(const std::string &filePath)
{
    auto it = std::find_if(m_wallMaterials.cbegin(), m_wallMaterials.cend(), [&filePath](const Material &material)
    {
        return material.filePath() == filePath;
    });

    if (it != m_wallMaterials.cend())
        return Geometry::MaterialId(std::distance(m_wallMaterials.cbegin(), it));

    m_wallMaterials.emplace_back(filePath);
    return Geometry::MaterialId(m_wallMaterials.size() - 1);
}

Geometry::MaterialId Level::addMaterial(const std::string &filePath, const sf::Color &taint)
{
    auto it = std::find_if(m_wallMaterials.cbegin(), m_wallMaterials.cend(), [&filePath, &taint](const Material &material)
    {
        return material.filePath() == filePath && material.taint() == taint;
    });

    if (it != m_wallMaterials.cend())
        return Geometry::MaterialId(std::distance(m_wallMaterials.cbegin(), it));

    m_wallMaterials.emplace_back(filePath);
    m_wallMaterials.back().applyTaint(taint);
    return Geometry::MaterialId(m_wallMaterials.size() - 1);
}

void Level::addGeometry(const Geometry &geo)
{
    m_geometries.push_back(geo);
}

void Level::addGeometries(const std::vector<Geometry> &geos)
{
    for (const Geometry &geo : geos)
        m_geometries.push_back(geo);
}

