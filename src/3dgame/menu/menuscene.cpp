#include "menuscene.h"

#include <iostream>
#include <memory>
#include <filesystem>

#include "widgets/button.h"


MenuScene::MenuScene(sf::RenderWindow &window, bool isGameRunning, const std::function<void(const GameStartConfig &)> &fnStartGame)
    : Scene(window),
      m_fnStartGame(fnStartGame)
{
    /*
    {
        auto model = std::make_shared<SelectionModelInt>();
        model->setMinMax(0, 13);
        auto selection = std::make_unique<Selection>(model);
        selection->onEnterPressed([model]{ std::cout << model->getValue() << "\n"; });
        m_formMain.addWidget(std::move(selection));
    }
    {
        std::vector<std::string> list { "AAA", "BBB", "CCC" };
        auto model = std::make_shared<SelectionModelList<std::string>>(list);
        auto selection = std::make_unique<Selection>(model);
        selection->onEnterPressed([model]{ std::cout << model->getValue() << "\n"; });
        m_formMain.addWidget(std::move(selection));
    }
    */

    if (isGameRunning)
    {
        auto buttContinueGame = std::make_unique<Button>("Continue Game");
        buttContinueGame->SignalEnterPressed::setCallback([this]
        {
            std::cout << "Continue Game\n";
            m_fnStartGame(GameStartConfig  { true, "" });
        });
        buttContinueGame->setDescription("Continue the game");
        m_formMain.addWidget(std::move(buttContinueGame));
    }

    {
        auto buttStartGame = std::make_unique<Button>("Start Game");
        buttStartGame->SignalEnterPressed::setCallback([this]
        {
            std::cout << "Starting Game\n";
            m_fnStartGame(GameStartConfig  { false, m_modelLevel->getValue() });
        });
        buttStartGame->setDescription("Start the game");
        m_formMain.addWidget(std::move(buttStartGame));
    }
    {
        namespace fs = std::filesystem;
        std::string path = "levels";

        std::vector<std::string> list;

        for (const auto &entry : fs::directory_iterator(path))
        {
            if (entry.is_directory())
            {
                list.push_back(entry.path().filename());
            }
        }

        m_modelLevel = std::make_shared<SelectionModelList<std::string>>(list);
        auto selection = std::make_unique<Selection>(m_modelLevel);
        //selection->onEnterPressed([this]{ m_fileNameLevel = m_modelLevel->getValue(); });
        selection->setDescription("Select level");
        selection->setLabel("Level:");
        m_formMain.addWidget(std::move(selection));
    }

//    {
//        auto model = std::make_shared<SelectionModelBool>();
//        auto selection = std::make_unique<Selection>(model);
//        selection->SignalEnterPressed::setCallback([model]
//        {

//        });
//        selection->setDescription("Fullscreen, yes or no?");
//        selection->setLabel("Fullscreen:");

//        m_formMain.addWidget(std::move(selection));
//    }

    {
        auto buttQuit = std::make_unique<Button>("Quit Game");
        buttQuit->SignalEnterPressed::setCallback([this]
        {
            std::cout << "Quit Game\n";
            m_window.close();
        });
        buttQuit->setDescription("Noooooo!!!!!");
        m_formMain.addWidget(std::move(buttQuit));
    }

    m_formMain.setFocused();
}

void MenuScene::update(std::chrono::duration<float> /*elapsed*/)
{
    sf::Event event;
    while (m_window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
            m_window.close();

        if (event.type == sf::Event::EventType::KeyPressed)
            m_formMain.input(event.key.code);
    }

//    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
//        m_window.close();

    m_window.clear();
    m_formMain.render(m_window);
    m_window.display();
}
