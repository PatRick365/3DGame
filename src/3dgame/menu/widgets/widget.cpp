#include "widget.h"

#include <iostream>

///
/// \brief WidgetSettings::WidgetSettings
///
WidgetSettings::WidgetSettings()
    : m_fontName("resources/hardpixel.otf"),
      m_fontSize(22),
      m_colorBase(sf::Color::White),
      m_colorSelected(sf::Color::Yellow)
{
    std::cout << "WidgetSettings initialized\n";
}

WidgetSettings &WidgetSettings::get()
{
    static WidgetSettings instance;
    return instance;
}

std::string WidgetSettings::fontName() const
{
    return m_fontName;
}

unsigned int WidgetSettings::fontSize() const
{
    return m_fontSize;
}

sf::Color WidgetSettings::colorBase() const
{
    return m_colorBase;
}

sf::Color WidgetSettings::colorSelected() const
{
    return m_colorSelected;
}

///
/// \brief Widget::Widget
///
void Widget::setFocused()
{
    m_hasFocus = true;
    setFocusedIntern();
}

sf::IntRect &Widget::bounds()
{
    return m_bounds;
}

std::string Widget::description() const
{
    return m_description;
}

void Widget::setDescription(const std::string &description)
{
    m_description = description;
}


