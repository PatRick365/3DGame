#include "selection.h"


Selection::Selection(const std::shared_ptr<SelectionModel> &model)
    : m_model(model)
{
    m_font.loadFromFile(WidgetSettings::get().fontName());

    m_bounds.height = 50;
    m_bounds.width = 300;

    m_text = m_model->fetch();

//    sf::Text textArrowLeft;
//    sf::Text textSelection;
//    sf::Text textArrowRight;


}

void Selection::render(sf::RenderWindow &window)
{
    std::string textText;

    if (!m_label.empty())
        textText += m_label + " ";

    sf::Text text(textText + "< " + m_text + " >", m_font, WidgetSettings::get().fontSize());
    text.setPosition(m_bounds.left + textArrowLeft.getGlobalBounds().left, m_bounds.top);
    window.draw(text);

    sf::FloatRect bounds = text.getLocalBounds();
    sf::RectangleShape rectFrame(sf::Vector2f(bounds.width + 10, bounds.height + 10));
    rectFrame.setFillColor(sf::Color::Transparent);
    sf::Color col = m_hasFocus ? WidgetSettings::get().colorSelected() : WidgetSettings::get().colorBase();
    rectFrame.setOutlineThickness(2);
    rectFrame.setOutlineColor(col);
    rectFrame.setPosition(bounds.left + m_bounds.left - 5, bounds.top + m_bounds.top - 5);
    window.draw(rectFrame);
}

void Selection::input(sf::Keyboard::Key key)
{
    switch (key)
    {
    case sf::Keyboard::Up:
    {
        m_hasFocus = false;
        SignalOnFocusExited::call(Navigation::Up);
        break;
    }
    case sf::Keyboard::Down:
    {
        m_hasFocus = false;
        SignalOnFocusExited::call(Navigation::Down);
        break;
    }
    case sf::Keyboard::Left:
    {
        m_text = m_model->fetchPrev();
        break;
    }
    case sf::Keyboard::Right:
    {
        m_text = m_model->fetchNext();
        break;
    }
    case sf::Keyboard::Enter:
    {
        SignalEnterPressed::call();
        break;
    }
    default:
        break;
    }
}

void Selection::setLabel(const std::string &text)
{
    m_label = text;
}
