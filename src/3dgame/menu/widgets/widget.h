#pragma once

#include <functional>
#include <optional>

#include <SFML/Graphics.hpp>

#include "signals.h"


class WidgetSettings
{
private:
    WidgetSettings();

public:
    WidgetSettings(const WidgetSettings &) = delete;
    WidgetSettings &operator=(const WidgetSettings &) = delete;
    WidgetSettings(WidgetSettings &&) = delete;
    WidgetSettings &operator=(WidgetSettings &&) = delete;

    static WidgetSettings &get();

    std::string fontName() const;
    unsigned int fontSize() const;

    sf::Color colorBase() const;
    sf::Color colorSelected() const;

private:
    std::string m_fontName;
    unsigned int m_fontSize;
    sf::Color m_colorBase;
    sf::Color m_colorSelected;
};


class Widget : public SignalOnFocusExited
{
public:
    virtual ~Widget() = default;

    virtual void render(sf::RenderWindow &window) = 0;
    virtual void input(sf::Keyboard::Key key) = 0;
    void onFocusExited(std::function<void (Navigation)> &&fn);

    void setPosition(const sf::Vector2i &pos) { m_bounds.left = pos.x; m_bounds.top = pos.y; }

    void setFocused();

    sf::IntRect &bounds();

    std::string description() const;
    void setDescription(const std::string &description);

protected:
    virtual void setFocusedIntern() = 0;

protected:
    sf::IntRect m_bounds;
    bool m_hasFocus = false;
    std::string m_description;


};

