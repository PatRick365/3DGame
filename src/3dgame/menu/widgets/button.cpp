#include "button.h"

Button::Button(const std::string &text)
    : m_text(text)
{
    m_font.loadFromFile(WidgetSettings::get().fontName());

    m_bounds.height = 50;
    m_bounds.width = 300;
}

void Button::render(sf::RenderWindow &window)
{   
    sf::Text text(m_text, m_font, WidgetSettings::get().fontSize());
    text.setPosition(m_bounds.left, m_bounds.top);
    window.draw(text);

    sf::FloatRect bounds = text.getLocalBounds();
    sf::RectangleShape rectFrame(sf::Vector2f(bounds.width + 10, bounds.height + 10));
    rectFrame.setFillColor(sf::Color::Transparent);
    sf::Color col = m_hasFocus ? WidgetSettings::get().colorSelected() : WidgetSettings::get().colorBase();
    rectFrame.setOutlineThickness(2);
    rectFrame.setOutlineColor(col);
    rectFrame.setPosition(bounds.left + m_bounds.left - 5, bounds.top + m_bounds.top - 5);
    window.draw(rectFrame);
}

void Button::input(sf::Keyboard::Key key)
{
    switch (key)
    {
    case sf::Keyboard::Up:
    {
        m_hasFocus = false;
        SignalOnFocusExited::call(Navigation::Up);
        break;
    }
    case sf::Keyboard::Down:
    {
        m_hasFocus = false;
        SignalOnFocusExited::call(Navigation::Down);
        break;
    }
    case sf::Keyboard::Enter:
    {
        SignalEnterPressed::call();
        break;
    }
    default:
        break;
    }
}

void Button::setFocusedIntern()
{
}
