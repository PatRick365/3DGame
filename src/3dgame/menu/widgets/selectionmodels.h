#pragma once

#include <string>
#include <vector>

extern std::string modelItemToString(const std::string &item);

class SelectionModel
{
public:
    virtual ~SelectionModel() = default;

    virtual std::string fetch() = 0;
    virtual std::string fetchNext() = 0;
    virtual std::string fetchPrev() = 0;
};

class SelectionModelInt : public SelectionModel
{
public:
    SelectionModelInt() = default;

    int getValue() const;

    void setValue(int value);
    void setMinMax(int min, int max);

    // SelectionModel interface
    std::string fetch() override;
    std::string fetchNext() override;
    std::string fetchPrev() override;

private:
    int m_value;
    int m_min;
    int m_max;
};

class SelectionModelBool : public SelectionModel
{
public:
    SelectionModelBool() = default;

    bool getValue() const;

    void setValue(bool value);

    // SelectionModel interface
    std::string fetch() override;
    std::string fetchNext() override;
    std::string fetchPrev() override;

private:
    bool m_value;
};

template<typename T>
class SelectionModelList : public SelectionModel
{
public:
    SelectionModelList(const std::vector<T> & list)
        : m_list(list),
          m_index(0)
    {}

    T getValue() const
    {
        return m_list[m_index];
    }

    // SelectionModel interface
    std::string fetch() override { return modelItemToString(m_list[m_index]); }
    std::string fetchNext() override
    {
        if (++m_index == m_list.size())
            m_index = 0;

        return modelItemToString(m_list[m_index]);
    }
    std::string fetchPrev() override
    {
        if (m_index == 0)
            m_index = m_list.size() - 1;
        else
            --m_index;

        return modelItemToString(m_list[m_index]);
    }

private:
    std::vector<T> m_list;
    size_t m_index;

};

