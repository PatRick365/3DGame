#pragma once

#include <string>

#include "widget.h"
#include "signals.h"


class Button : public Widget, public SignalEnterPressed
{
public:
    Button(const std::string &text);

    // Widget interface
public:
    void render(sf::RenderWindow &window) override;
    void input(sf::Keyboard::Key key) override;

protected:
    void setFocusedIntern() override;

private:
    sf::Font m_font;
    std::string m_text;
};

