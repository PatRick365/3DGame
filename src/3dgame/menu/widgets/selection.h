#pragma once

#include <string>
#include <memory>

#include "widget.h"
#include "signals.h"
#include "selectionmodels.h"

class Selection : public Widget, public SignalEnterPressed
{
public:
    Selection(const std::shared_ptr<SelectionModel> &model);

    // Widget interface
    void render(sf::RenderWindow &window) override;
    void input(sf::Keyboard::Key key) override;

    void setLabel(const std::string &text);

protected:
    void setFocusedIntern() override
    {
    }

private:
    sf::Text textArrowLeft;
    sf::Text textSelection;
    sf::Text textArrowRight;

    sf::Font m_font;
    std::string m_text;
    std::string m_label;
    std::shared_ptr<SelectionModel> m_model;
};

