#pragma once

#include <functional>
#include <future>


template<typename Signature>
class Signal
{
public:
    virtual ~Signal() = default;

    void setCallback(std::function<Signature> &&fn)
    {
        m_fn = std::move(fn);
        m_async = false;
    }

    void setCallbackAsync(std::function<Signature> &&fn)
    {
        m_fn = std::move(fn);
        m_async = true;
    }

protected:
    template<typename ...Args>
    void call(Args &&...args)
    {
        if (m_fn)
        {
            if (m_async)
            {
                m_futureStorage.push_back(std::move(std::async(std::launch::async, m_fn, std::forward<Args>(args)...)));
            }
            else
            {
                m_fn(std::forward<Args>(args)...);
            }
        }
    }

private:
    std::function<Signature> m_fn;
    bool m_async = false;

    std::vector<std::future<void>> m_futureStorage;
};

class SignalEnterPressed : public Signal<void(void)> {};

enum class Navigation { Up, Down };
class SignalOnFocusExited : public Signal<void(Navigation)> {};
