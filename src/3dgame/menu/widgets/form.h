#pragma once

#include "widget.h"


class Form : public Widget
{
public:
    Form();

    void addWidget(std::unique_ptr<Widget> widget);

    // Widget interface
    void render(sf::RenderWindow &window) override;
    void input(sf::Keyboard::Key key) override;

protected:
    void setFocusedIntern() override;

private:
    void widgetExitedFocus(Navigation navigation);

private:
    sf::Font m_font;
    size_t m_indexFocusedWidget;
    std::vector<std::unique_ptr<Widget>> m_widgets;
};

