#include "selectionmodels.h"

#include <stdexcept>


std::string modelItemToString(const std::string &item)
{
    return item;
}

int SelectionModelInt::getValue() const
{
    return m_value;
}

void SelectionModelInt::setValue(int value)
{
    if (value < m_min)
        m_value = m_max;
    else if (value > m_max)
        m_value = m_min;
    else
        m_value = value;
}

void SelectionModelInt::setMinMax(int min, int max)
{
    if (min > max)
        throw std::logic_error("min > max");

    m_min = min;
    m_max = max;
}

std::string SelectionModelInt::fetch()
{
    return std::to_string(m_value);
}

std::string SelectionModelInt::fetchNext()
{
    setValue(m_value + 1);
    return std::to_string(m_value);
}

std::string SelectionModelInt::fetchPrev()
{
    setValue(m_value - 1);
    return std::to_string(m_value);
}


bool SelectionModelBool::getValue() const
{
    return m_value;
}

void SelectionModelBool::setValue(bool value)
{
    m_value = value;
}

std::string SelectionModelBool::fetch()
{
    return m_value ? "yes" : "no";
}

std::string SelectionModelBool::fetchNext()
{
    m_value = !m_value;
    return fetch();
}

std::string SelectionModelBool::fetchPrev()
{
    return fetchNext();;
}
