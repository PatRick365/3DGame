#include "form.h"

Form::Form()
    : m_indexFocusedWidget(0)
{
    m_font.loadFromFile(WidgetSettings::get().fontName());
}

void Form::addWidget(std::unique_ptr<Widget> widget)
{
    using namespace std::placeholders;

    widget->SignalOnFocusExited::setCallback(std::bind(&Form::widgetExitedFocus, this, _1));

    m_widgets.push_back(std::move(widget));
}

void Form::render(sf::RenderWindow &window)
{
    sf::Vector2i posLayout(50, 50);

    for (auto &widget : m_widgets)
    {
        widget->setPosition(posLayout);
        posLayout.y = widget->bounds().top + widget->bounds().height + 5;
    }

    for (const auto &widget : m_widgets)
    {
        widget->render(window);
    }

    sf::Vector2f pos(m_widgets.back()->bounds().left, m_widgets.back()->bounds().top + 300);

    if (!m_widgets.empty() && !m_widgets[m_indexFocusedWidget]->description().empty())
    {
        sf::Text text(m_widgets[m_indexFocusedWidget]->description(), m_font, WidgetSettings::get().fontSize());
        text.setPosition(pos.x, pos.y);
        window.draw(text);

//        sf::FloatRect bounds = text.getLocalBounds();
//        sf::RectangleShape rectFrame(sf::Vector2f(bounds.width + 10, bounds.height + 10));
//        rectFrame.setFillColor(sf::Color::Transparent);
//        sf::Color col = sf::Color::Green;
//        rectFrame.setOutlineThickness(2);
//        rectFrame.setOutlineColor(col);
//        rectFrame.setPosition(text.getGlobalBounds().left, text.getGlobalBounds().top);
//        window.draw(rectFrame);
    }
}

void Form::input(sf::Keyboard::Key key)
{
    if (!m_widgets.empty())
    {
        m_widgets.at(m_indexFocusedWidget)->input(key);
    }

}

void Form::setFocusedIntern()
{
    if (!m_widgets.empty())
    {
        // bunch a dead code
//        auto itWidest = std::max_element(m_widgets.cbegin(), m_widgets.cend(), [](const std::unique_ptr<Widget> &widget1, const std::unique_ptr<Widget> &widget2)
//        {
//            return widget1->bounds().width < widget2->bounds().width;
//        });

//        for (auto &widget : m_widgets)
//            widget->bounds().width = itWidest->get()->bounds().width;

        m_widgets.front()->setFocused();
        m_indexFocusedWidget = 0;
    }
}

void Form::widgetExitedFocus(Navigation navigation)
{
    if (navigation == Navigation::Up)
    {
        if (m_indexFocusedWidget == 0)
            m_indexFocusedWidget = m_widgets.size() - 1;
        else
            --m_indexFocusedWidget;
    }
    else if (navigation == Navigation::Down)
    {
        ++m_indexFocusedWidget;

        if (m_indexFocusedWidget == m_widgets.size())
            m_indexFocusedWidget = 0;
    }

    m_widgets.at(m_indexFocusedWidget)->setFocused();
}


