#pragma once

#include <memory>
#include <functional>

#include "scene.h"
#include "widgets/form.h"
#include "widgets/selection.h"


struct GameStartConfig
{   
    bool continueGame;
    std::string levelPath;
};

class MenuScene : public Scene
{
public:
    MenuScene(sf::RenderWindow &window, bool isGameRunning, const std::function<void(const GameStartConfig &)> &fnStartGame);

    void update(std::chrono::duration<float> elapsed) override;

private:
    Form m_formMain;
    bool m_isContinue = false;
    std::shared_ptr<SelectionModelList<std::string>> m_modelLevel;
    std::function<void(const GameStartConfig &)> m_fnStartGame;
};
