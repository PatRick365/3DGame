#include "raycaster.h"

#define FMT_HEADER_ONLY
#include "../extern/fmt/format.h"

RayCaster::RayCaster(std::vector<Geometry> &geometries)
    : m_geometries(geometries)
{}

std::vector<RayCaster::Collision> RayCaster::calcIntersections(sf::Vector2f rayBegin, sf::Vector2f rayEnd)
{
    std::vector<RayCaster::Collision> collisions;

    float angleRay = QuickMath::angleOfLine(rayBegin, rayEnd);

    for (Geometry &geo : m_geometries)
    {
        float angleWall = QuickMath::angleOfLine(geo.start(), geo.end());
        float angleDirect = angleWall + QuickMath::pi / 2.0f;
        float angleDiff = QuickMath::angleDiff(angleRay, angleDirect);
        bool valid = std::abs(angleDiff) < QuickMath::pi / 2.0f;

        if (!valid/* && !geo.isTransparent()*/)
        {
            //fmt::print("not valid: rayAngle({:.{}f}) wallAngle: {:.{}f} diffAngle: {:.{}f}\n", rayAngle, 2, wallAngle, 2, diffAngle, 2);
            continue;
        }
        //fmt::print("valid: rayAngle({:.{}f}) wallAngle: {:.{}f} diffAngle: {:.{}f}\n", rayAngle, 2, wallAngle, 2, diffAngle, 2);

        if (QuickMath::doIntersect(rayBegin, rayEnd, geo.start(), geo.end()))
        {
            sf::Vector2f vecIntersect = QuickMath::lineLineIntersection(rayBegin, rayEnd, geo.start(), geo.end());

            float distance = QuickMath::vectorLength(rayBegin - vecIntersect);

            RayCaster::Collision collision;
            collision.intersect = vecIntersect;
            collision.distance = distance;
            collision.geo = &geo;
            collision.fSampleX = QuickMath::vectorLength(vecIntersect - geo.start());
            collisions.push_back(std::move(collision));
        }
    }

    std::sort(collisions.begin(), collisions.end(), [](const Collision &col1, const Collision &col2){ return col1.distance < col2.distance; });

    auto itFirstOpaque = std::find_if(collisions.begin(), collisions.end(), [](const Collision &col){ return !col.geo->isTransparent(); });

    if (itFirstOpaque != collisions.end()/* && itFirstOpaque + 1 != collisions.end()*/)
    {
        collisions.erase(itFirstOpaque + 1, collisions.end());
    }

    std::reverse(collisions.begin(), collisions.end());

    //std::cout << "blockside: " << size_t(collisions.front().blockSide) << " dist: " << collisions.front().distance << "\n";
//    std::cout << "cols: " << collisions.size() << " ids: ";
//    for (const auto &col : collisions)
//    {
//        std::cout << col.fSampleX << "(" << col.geo->isTransparent() << ")" << ", ";
//    }
//    std::cout << "\n";

    return collisions;
}

std::optional<sf::Vector2f> RayCaster::calcClosestIntersection(sf::Vector2f rayBegin, sf::Vector2f rayEnd, float radius)
{
    float angleRay = QuickMath::angleOfLine(rayBegin, rayEnd);

    float distSquaredNearest = 0;
    std::optional<sf::Vector2f> intersectNearest;

    for (Geometry &geo : m_geometries)
    {
        float angleWall = QuickMath::angleOfLine(geo.start(), geo.end());
        float angleDirect = angleWall + QuickMath::pi / 2.0f;
        float angleDiff = QuickMath::angleDiff(angleRay, angleDirect);
        bool valid = std::abs(angleDiff) < QuickMath::pi / 2.0f;

        if (!valid)
        {
            //fmt::print("not valid: rayAngle({:.{}f}) wallAngle: {:.{}f} diffAngle: {:.{}f}\n", rayAngle, 2, wallAngle, 2, diffAngle, 2);
            continue;
        }
        //fmt::print("valid: rayAngle({:.{}f}) wallAngle: {:.{}f} diffAngle: {:.{}f}\n", rayAngle, 2, wallAngle, 2, diffAngle, 2);

        if (QuickMath::doIntersect(rayBegin, rayEnd, geo.start(), geo.end()))
        {
            sf::Vector2f vecIntersect = QuickMath::lineLineIntersection(rayBegin, rayEnd, geo.start(), geo.end());

            float distSquared = QuickMath::vectorLengthSquared(rayBegin - vecIntersect);

            if (!intersectNearest.has_value() || distSquared < distSquaredNearest)
            {
                intersectNearest = vecIntersect;
            }
        }
    }

    return intersectNearest;
}
