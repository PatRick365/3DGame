#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <chrono>
#include <cstdio>
#include <cmath>

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

constexpr int resX = 1200;
constexpr int resY = 800;

constexpr int nScreenWidth = resX / 4;			// Console Screen Size X (columns)
constexpr int nScreenHeight = resY / 4;			// Console Screen Size Y (rows)
constexpr int nMapWidth = 16;				// World Dimensions
constexpr int nMapHeight = 16;

constexpr float fFOV = 3.14159f / 4.0f;	// Field of View
constexpr float fDepth = 16.0f;			// Maximum rendering distance
constexpr float fSpeed = 5.0f;			// Walking Speed

constexpr int pixelWidth = resX / nScreenWidth;
constexpr int pixelHeight = resY / nScreenHeight;

constexpr float rad90 = 1.5708f;

sf::Color operator*(const sf::Color &lhs, float rhs)
{
    return sf::Color(lhs.r * rhs, lhs.g * rhs, lhs.b * rhs);
}

int main()
{
    //char *screen = new wchar_t[nScreenWidth*nScreenHeight];
    std::array<sf::Color, nScreenWidth * nScreenHeight> screen;

    sf::RenderWindow window(sf::VideoMode(resX, resY), "3DGame");

    sf::Vector2f playerPos(14.7f, 5.09f);
    float playerAngle = 0.0f;			// Player Start Rotation

    sf::Color colFloor = sf::Color::Blue;
    sf::Color colWall = sf::Color::Red;

    // Create Map of world space # = wall block, . = space
    std::string map;
    map += "################";
    map += "#..............#";
    map += "#.......########";
    map += "#..............#";
    map += "#......##......#";
    map += "#......##......#";
    map += "#..............#";
    map += "###............#";
    map += "##.............#";
    map += "#......####..###";
    map += "#......#.......#";
    map += "#......#.......#";
    map += "#..............#";
    map += "#......#########";
    map += "#..............#";
    map += "################";

    sf::String mapDisplay;
    for (size_t i = 0; i < map.size(); i += nMapWidth)
    {
        if (!mapDisplay.isEmpty())
            mapDisplay += '\n';
        mapDisplay += map.substr(i, nMapWidth);
    }

    sf::Font mapFont;
    if (!mapFont.loadFromFile("computo-monospace.otf"))
    {
        std::cerr << "failed to load font\n";
        return 1;
    }

    auto tp1 = std::chrono::high_resolution_clock::now();
    auto tp2 = std::chrono::high_resolution_clock::now();

    while (window.isOpen())
    {
        // We'll need time differential per frame to calculate modification
        // to movement speeds, to ensure consistant movement, as ray-tracing
        // is non-deterministic
        tp2 = std::chrono::system_clock::now();
        std::chrono::duration<float> elapsedTime = tp2 - tp1;
        tp1 = tp2;
        float fElapsedTime = elapsedTime.count();

        sf::Event event;
        while (window.pollEvent(event))
        {
            // Close window: exit
            if (event.type == sf::Event::Closed)
                window.close();
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
            window.close();

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) ^ sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            {
                playerAngle -= (fSpeed * 0.75f) * fElapsedTime;
            }
            else
            {
                playerAngle += (fSpeed * 0.75f) * fElapsedTime;
            }
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) ^ sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
            {
                float fPlayerXBefore = playerPos.x;
                float fPlayerYBefore = playerPos.y;

                playerPos.x += sinf(playerAngle) * fSpeed * fElapsedTime;
                playerPos.y += cosf(playerAngle) * fSpeed * fElapsedTime;
                if (map.c_str()[(int)playerPos.x * nMapWidth + (int)playerPos.y] == '#')
                {
                    playerPos.x = fPlayerXBefore;
                    playerPos.y = fPlayerYBefore;
                }
            }
            else
            {
                float fPlayerXBefore = playerPos.x;
                float fPlayerYBefore = playerPos.y;

                playerPos.x -= sinf(playerAngle) * fSpeed * fElapsedTime;
                playerPos.y -= cosf(playerAngle) * fSpeed * fElapsedTime;
                if (map.c_str()[(int)playerPos.x * nMapWidth + (int)playerPos.y] == '#')
                {
                    playerPos.x = fPlayerXBefore;
                    playerPos.y = fPlayerYBefore;
                }
            }
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Y) ^ sf::Keyboard::isKeyPressed(sf::Keyboard::X))
        {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Y))
            {
                float fPlayerXBefore = playerPos.x;
                float fPlayerYBefore = playerPos.y;

                playerPos.x += sinf(playerAngle - rad90) * fSpeed * fElapsedTime;
                playerPos.y += cosf(playerAngle - rad90) * fSpeed * fElapsedTime;
                if (map.c_str()[(int)playerPos.x * nMapWidth + (int)playerPos.y] == '#')
                {
                    playerPos.x = fPlayerXBefore;
                    playerPos.y = fPlayerYBefore;
                }
            }
            else
            {
                float fPlayerXBefore = playerPos.x;
                float fPlayerYBefore = playerPos.y;

                playerPos.x += sinf(playerAngle + rad90) * fSpeed * fElapsedTime;
                playerPos.y += cosf(playerAngle + rad90) * fSpeed * fElapsedTime;
                if (map.c_str()[(int)playerPos.x * nMapWidth + (int)playerPos.y] == '#')
                {
                    playerPos.x = fPlayerXBefore;
                    playerPos.y = fPlayerYBefore;
                }
            }
        }


        for (int x = 0; x < nScreenWidth; x++)
        {
            // For each column, calculate the projected ray angle into world space
            float fRayAngle = (playerAngle - fFOV/2.0f) + ((float)x / (float)nScreenWidth) * fFOV;

            // Find distance to wall
            float fStepSize = 0.01f;		  // Increment size for ray casting, decrease to increase
            float fDistanceToWall = 0.0f; //                                      resolution

            bool bHitWall = false;		// Set when ray hits wall block
            bool bBoundary = false;		// Set when ray hits boundary between two wall blocks

            float fEyeX = ::sinf(fRayAngle); // Unit vector for ray in player space
            float fEyeY = ::cosf(fRayAngle);

            // Incrementally cast ray from player, along ray angle, testing for
            // intersection with a block
            while (!bHitWall && fDistanceToWall < fDepth)
            {
                fDistanceToWall += fStepSize;
                int nTestX = (int)(playerPos.x + fEyeX * fDistanceToWall);
                int nTestY = (int)(playerPos.y + fEyeY * fDistanceToWall);

                // Test if ray is out of bounds
                if (nTestX < 0 || nTestX >= nMapWidth || nTestY < 0 || nTestY >= nMapHeight)
                {
                    bHitWall = true;			// Just set distance to maximum depth
                    fDistanceToWall = fDepth;
                }
                else
                {
                    // Ray is inbounds so test to see if the ray cell is a wall block
                    //if (map.c_str()[nTestX * nMapWidth + nTestY] == '#')
                    if (map.c_str()[nTestX * nMapWidth + nTestY] == '#')
                    {
                        // Ray has hit wall
                        bHitWall = true;

                        // To highlight tile boundaries, cast a ray from each corner
                        // of the tile, to the player. The more coincident this ray
                        // is to the rendering ray, the closer we are to a tile
                        // boundary, which we'll shade to add detail to the walls
                        std::vector<std::pair<float, float>> p;

                        // Test each corner of hit tile, storing the distance from
                        // the player, and the calculated dot product of the two rays
                        for (int tx = 0; tx < 2; tx++)
                        {
                            for (int ty = 0; ty < 2; ty++)
                            {
                                // Angle of corner to eye
                                float vy = (float)nTestY + ty - playerPos.y;
                                float vx = (float)nTestX + tx - playerPos.x;
                                float d = ::sqrt(vx*vx + vy*vy);
                                float dot = (fEyeX * vx / d) + (fEyeY * vy / d);
                                p.push_back(std::make_pair(d, dot));
                            }
                        }

                        // Sort Pairs from closest to farthest
                        std::sort(p.begin(), p.end(), [](const std::pair<float, float> &left, const std::pair<float, float> &right) {return left.first < right.first; });

                        // First two/three are closest (we will never see all four)
                        float fBound = 0.01;
                        if (acos(p.at(0).second) < fBound)
                            bBoundary = true;
                        if (acos(p.at(1).second) < fBound)
                            bBoundary = true;
                        if (acos(p.at(2).second) < fBound)
                            bBoundary = true;
                    }
                }
            }

            // Calculate distance to ceiling and floor
            int nCeiling = (nScreenHeight / 2.0) - nScreenHeight / fDistanceToWall;
            int nFloor = nScreenHeight - nCeiling;

            // Shader walls based on distance
            sf::Color nShade = sf::Color::Black;

            nShade = sf::Color::Black + (colWall * (1 - (fDistanceToWall / fDepth)));

            if (bBoundary)
                nShade = sf::Color::Black; // Black it out

            for (int y = 0; y < nScreenHeight; y++)
            {
                // Each Row
                if(y <= nCeiling)
                    screen[y*nScreenWidth + x] = sf::Color::Black;
                else if(y > nCeiling && y <= nFloor)
                    screen[y*nScreenWidth + x] = nShade;
                else // Floor
                {
                    // Shade floor based on distance
                    float b = 1.0f - (y - nScreenHeight / 2.0f) / (nScreenHeight / 2.0f);

                    nShade = sf::Color::Black + (colFloor * (1 - b));

                    screen[y*nScreenWidth + x] = nShade;
                }
            }
        }

        // Display Stats
        //swprintf_s(screen, 40, L"X=%3.2f, Y=%3.2f, A=%3.2f FPS=%3.2f ", posPlayer.x, posPlayer.y, fPlayerA, 1.0f/fElapsedTime);

        // Display Map
//        for (int nx = 0; nx < nMapWidth; nx++)
//            for (int ny = 0; ny < nMapWidth; ny++)
//            {
//                screen[(ny+1)*nScreenWidth + nx] = map[ny * nMapWidth + nx];
//            }
//        screen[((int)posPlayer.x+1) * nScreenWidth + (int)posPlayer.y] = 'P';

        // Display Frame
//        screen[nScreenWidth * nScreenHeight - 1] = '\0';
//        WriteConsoleOutputCharacter(hConsole, screen, nScreenWidth * nScreenHeight, { 0,0 }, &dwBytesWritten);

        // Clear screen
        window.clear();

        sf::RectangleShape rect(sf::Vector2f(pixelWidth, pixelHeight));

        for (int y = 0; y < nScreenHeight; ++y)
        {
            for (int x = 0; x < nScreenWidth; ++x)
            {
                rect.setFillColor(screen[nScreenWidth * y + x]);
                rect.setPosition(x * pixelWidth, y * pixelHeight);
                window.draw(rect);
            }
        }

        std::string mapDisplayCopy = mapDisplay;

        mapDisplayCopy[(int)playerPos.x * (nMapWidth + 1) + (int)playerPos.y] = 'P';
        sf::Text mapText(mapDisplayCopy, mapFont, 20);
        window.draw(mapText);

        // Update the window
        window.display();

    }

    return 0;
}

// That's It!! - Jx9

