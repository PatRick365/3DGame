#include "application.h"

Application::Application()
    : m_window(sf::VideoMode(1200, 800), "3DGame"),
      m_sceneActive(nullptr),
      m_isGameRunning(false)

{
    switchToMenuScene();
}

bool Application::run()
{
    auto tp1 = std::chrono::high_resolution_clock::now();
    auto tp2 = std::chrono::high_resolution_clock::now();

    while (m_window.isOpen())
    {
        tp2 = std::chrono::system_clock::now();
        std::chrono::duration<float> elapsedTime = tp2 - tp1;
        tp1 = tp2;

        m_sceneActive->update(elapsedTime);
    }

    return true;
}

void Application::switchToMenuScene()
{
    using namespace std::placeholders;

    m_sceneMenu = std::make_unique<MenuScene>(m_window, m_isGameRunning, std::bind(&Application::switchToGameScene, this, _1));
    m_sceneActive = m_sceneMenu.get();
}

void Application::switchToGameScene(const GameStartConfig &gameStartConfig)
{
    if (!gameStartConfig.continueGame)
    {
        m_sceneGame = std::make_unique<GameScene>(m_window, std::bind(&Application::switchToMenuScene, this));

        Level::LoadResult loadResult = m_sceneGame->loadLevel(gameStartConfig.levelPath);

        if (!loadResult.success)
        {
            std::cerr << "ERROR: " << loadResult.error << "\n";
            return;
        }
    }

    m_sceneActive = m_sceneGame.get();
    m_isGameRunning = true;
}
