#include "material.h"

#include <algorithm>


static bool isTransparent(sf::Image &image)
{
    for (unsigned int y = 0; y < image.getSize().y; ++y)
    {
        for (unsigned int x = 0; x < image.getSize().x; ++x)
        {
            if (image.getPixel(x, y).a < 255)
                return true;
        }
    }
    return false;
}

Material::Material(const std::string &filePath)
    : m_filePath(filePath)
{
    m_image.loadFromFile(filePath);

    m_isTransparent = ::isTransparent(m_image);
}

const sf::Image &Material::image() const
{
    return m_image;
}

void Material::applyTaint(const sf::Color &color)
{
    m_taint = color;

    for (unsigned int y = 0; y < m_image.getSize().y; ++y)
    {
        for (unsigned int x = 0; x < m_image.getSize().x; ++x)
        {
            sf::Color colorOld = m_image.getPixel(x, y);
            colorOld.r = std::min(colorOld.r + color.r, 255);
            colorOld.g = std::min(colorOld.g + color.g, 255);
            colorOld.b = std::min(colorOld.b + color.b, 255);
            m_image.setPixel(x, y, colorOld);
        }
    }
}

bool Material::isTransparent() const
{
    return m_isTransparent;
}

std::string Material::filePath() const
{
    return m_filePath;
}

sf::Color Material::taint() const
{
    return m_taint;
}
