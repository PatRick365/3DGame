#pragma once

#include <memory>
#include <vector>

#include "entt/entt.hpp"

#include "scene.h"
#include "level.h"
#include "systems/isystem.h"


class GameScene : public Scene
{
public:
    GameScene(sf::RenderWindow &window, const std::function<void(void)> &fnPauseGame);

    void update(std::chrono::duration<float> elapsed) override;

    Level::LoadResult loadLevel(const std::string &levelPath);

private:
    Level m_level;

    entt::registry m_registry;
    std::vector<std::unique_ptr<System::ISystem>> m_systems;

    std::function<void(void)> m_fnPauseGame;
};

