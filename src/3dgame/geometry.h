#pragma once

#include <array>
#include <cstddef>

#include <SFML/Graphics.hpp>

#include "wrappedtype.hpp"


class Geometry
{
public:
    //enum class Side { West = 0, North, East, South };
    enum class MaterialId : size_t { };

    static std::vector<Geometry> make(const sf::Vector2f &pos, const sf::Vector2f &size, MaterialId materialId);

    Geometry() = default;
    Geometry(const sf::Vector2f &start, const sf::Vector2f &end, MaterialId materialId);

    sf::Vector2f start() const;
    sf::Vector2f end() const;

    MaterialId materialId() const;
    bool isSolid() const;
    void setIsSolid(bool isSolid);

    bool isTransparent() const;
    void setIsTransparent(bool isTransparent);

private:
    sf::Vector2f m_start;
    sf::Vector2f m_end;
    MaterialId m_materialId = MaterialId(0);
    bool m_isSolid = true;
    bool m_isTransparent = false;
};



