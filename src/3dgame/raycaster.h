#pragma once

#include <vector>
#include <utility>
#include <array>
#include <optional>

#include "geometry.h"
#include "quickmath.h"


class RayCaster
{
public:
    struct Collision
    {
        Geometry *geo;
        float fSampleX;
        float distance;
        sf::Vector2f intersect;
    };

    RayCaster(std::vector<Geometry> &geometries);

    std::vector<RayCaster::Collision> calcIntersections(sf::Vector2f rayBegin, sf::Vector2f rayEnd);

    std::optional<sf::Vector2f> calcClosestIntersection(sf::Vector2f rayBegin, sf::Vector2f rayEnd, float radius);

private:
    std::vector<Geometry> &m_geometries;
};

