#pragma once

#include <SFML/Graphics.hpp>


class GlobalSettings
{
private:
    GlobalSettings();

public:
    GlobalSettings(const GlobalSettings &) = delete;
    GlobalSettings &operator=(const GlobalSettings &) = delete;
    GlobalSettings(GlobalSettings &&) = delete;
    GlobalSettings &operator=(GlobalSettings &&) = delete;

    static GlobalSettings &get()
    {
        static GlobalSettings instance;
        return instance;
    }
    sf::Vector2u resolution() const;

private:
    sf::Vector2u m_resolution = { 1200, 800 };
};
