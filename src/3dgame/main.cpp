#include "application.h"

int main()
{
    Application app;

    return app.run() ? 0 : 1;
}
