#pragma once

#include <vector>
#include <memory>
#include <stack>

#include <SFML/Graphics.hpp>

#include "systems/isystem.h"
#include "level.h"
#include "scene.h"
#include "gamescene.h"
#include "menu/menuscene.h"


class Application
{
public:
    Application();

    bool run();

    //std::function<void(GameStartConfig)>

    void switchToMenuScene();
    void switchToGameScene(const GameStartConfig &gameStartConfig);

private:
    sf::RenderWindow m_window;

    Scene *m_sceneActive;
    std::unique_ptr<MenuScene> m_sceneMenu;
    std::unique_ptr<GameScene> m_sceneGame;

    bool m_isGameRunning;
};

