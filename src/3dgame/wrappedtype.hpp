#pragma once

#include <utility>

template<typename T>
class WrappedType
{
public:
    WrappedType(T &&val)
        : value(std::move(val))
    {}
//protected:
//    ~WrappedType() = default;

public:
    T value;
};

