#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <iostream>

#include <SFML/Graphics.hpp>

#include "geometry.h"
#include "material.h"


class Level
{
public:
    struct LoadResult
    {
    private:
        LoadResult() = default;

    public:
        static LoadResult makeError(const std::string &strError)
        {
            return LoadResult { false, strError };
        }

        static LoadResult makeOk()
        {
            return LoadResult { true, std::string() };
        }

        bool success = false;
        std::string error = "empty error";
    };

    Level() = default;

    LoadResult loadFromDisk(const std::string &folderName);
    void loadIt();

    size_t w() const;
    size_t h() const;

    const sf::Color &colorFloor() const;
    const sf::Color &colorCeiling() const;

    sf::Vector2f playerStartPos() const;
    float playerStartAngle() const;

    const std::vector<Material> &wallTextures() const;
    Material &wallTexture(Geometry::MaterialId id);

    Geometry::MaterialId addMaterial(const std::string &filePath);
    Geometry::MaterialId addMaterial(const std::string &filePath, const sf::Color &taint);

    void addGeometry(const Geometry &geo);
    void addGeometries(const std::vector<Geometry> &geos);

    std::vector<Geometry> m_geometries;
    sf::FloatRect m_bounds = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(9, 9));

private:
    std::vector<Material> m_wallMaterials;

    sf::Color m_colorFloor;
    sf::Color m_colorCeiling;
    sf::Vector2f m_playerStartPos;
    float m_playerStartAngle;
};

