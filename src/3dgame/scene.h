#pragma once

#include <chrono>

#include <SFML/Graphics.hpp>


class Scene
{
public:
    Scene(sf::RenderWindow &window);
    virtual ~Scene() = default;

    virtual void update(std::chrono::duration<float> elapsed) = 0;

protected:
    sf::RenderWindow &m_window;
};

