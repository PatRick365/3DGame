#include "gamescene.h"

#include "systems/movement.h"
#include "systems/graphics.h"
#include "systems/controls.h"
#include "systems/debug.h"

#include "components.h"

#include "quickmath.h"
#include <iomanip>
void test()
{
    std::cout << std::boolalpha << std::setprecision(3);

    std::cout << "up: " << QuickMath::angleOfLine(sf::Vector2f(0, 0), sf::Vector2f(0, -1)) << "\n";
    std::cout << "right: " << QuickMath::angleOfLine(sf::Vector2f(0, 0), sf::Vector2f(1, 0)) << "\n";
    std::cout << "down: " << QuickMath::angleOfLine(sf::Vector2f(0, 0), sf::Vector2f(0, 1)) << "\n";
    std::cout << "left: " << QuickMath::angleOfLine(sf::Vector2f(0, 0), sf::Vector2f(-1, 0)) << "\n";
    std::cout << "left: " << QuickMath::angleOfLine(sf::Vector2f(1, 0), sf::Vector2f(0, 0)) << "\n";

    std::cout << "up_right: " << QuickMath::angleOfLine(sf::Vector2f(0, 0), sf::Vector2f(1, -1)) << "\n";

    std::cout << "\n";

    //float aW = QuickMath::angleOfLineRad(sf::Vector2f(0, 0), sf::Vector2f(1, 0));
    float aW = QuickMath::angleOfLine(sf::Vector2f(0, 0), sf::Vector2f(1, 0));

    std::cout << "wall angle: " << aW << "\n";

//    float lower = (QuickMath::pi / 4.0f);
//    float upper = ((QuickMath::pi / 4.0f) * 3.0f);
//    float lower = -(QuickMath::pi / 2.0f);
//    float upper = -lower;

    //std::cout << "lower: " << lower << ", upper: " << upper << "\n";

    auto getDiff = [aW/*, lower, upper*/](const std::string &str, sf::Vector2f vecPStart, sf::Vector2f vecPEnd) -> float
    {
        float aP = QuickMath::angleOfLine(vecPStart, vecPEnd);

        //aP += QuickMath::pi / 2;

        float diff = (aW + 10) - (aP + 10);
        diff = std::abs(diff);
        //        diff -= QuickMath::pi / 2;

        //        bool valid = diff >= lower && diff <= upper;

        bool valid = diff < QuickMath::pi;

        std::cout << str << ": angle: " << aP << " diff: " << diff << " valid: " << valid << "\n";

        return diff;
};

    //lower: -1.5708, upper: 1.5708
    //aStraight: angle: 1.5708 diff: 3.14159 valid: false
    //aLeft: angle: 3.14159 diff: 1.5708 valid: true
    //aRight: angle: 0 diff: 4.71239 valid: false
    //aDigLeft: angle: 2.35619 diff: 2.35619 valid: false
    //aDigRight: angle: 0.785398 diff: 3.92699 valid: false
    //aStraightDown: angle: 4.71239 diff: 0 valid: true


    getDiff("aStraight", sf::Vector2f(0, 0), sf::Vector2f(0, -1));
    getDiff("aLeft", sf::Vector2f(0, 0), sf::Vector2f(1, 0));
    getDiff("aRight", sf::Vector2f(0, 0), sf::Vector2f(-1, 0));

    getDiff("aDigLeft", sf::Vector2f(0, 0), sf::Vector2f(1, -1));
    getDiff("aDigRight", sf::Vector2f(0, 0), sf::Vector2f(-1, -1));

    getDiff("aStraightDown", sf::Vector2f(0, 0), sf::Vector2f(0, 1));

    //std::exit(0);
}


//void test()
//{
//    float aW = QuickMath::angleOfLineRad(sf::Vector2f(0, 0), sf::Vector2f(1, 0));

//    float lower = (QuickMath::pi / 4.0f);
//    float upper = ((QuickMath::pi / 4.0f) * 3.0f);

//    std::cout << "lower: " << lower << ", upper: " << upper << "\n";

//    auto getDiff = [aW, lower, upper](const std::string &str, sf::Vector2f vecP) -> float
//    {
//        float aP = QuickMath::angleOfLineRad(sf::Vector2f(0, 0), vecP);

//        float diff = aW - aP;

//        bool valid = diff >= lower && diff <= upper;

//        std::cout << std::boolalpha;
//        std::cout << str << ": diff: " << diff << " valid: " << valid << "\n";

//        return diff;
//    };

//    //aW: 4.71239
//    //aStraight: 1.5708
//    //aLeft: 0.785398
//    //aRight: 2.35619

//    getDiff("aStraight", sf::Vector2f(0, -1));
//    getDiff("aLeft", sf::Vector2f(1, 0));
//    getDiff("aRight", sf::Vector2f(-1, 0));

//    getDiff("aDigLeft", sf::Vector2f(1, -1));
//    getDiff("aDigRight", sf::Vector2f(-1, -1));

//    getDiff("aStraightDown", sf::Vector2f(0, 1));

//    //std::exit(0);
//}

GameScene::GameScene(sf::RenderWindow &window, const std::function<void(void)> &fnPauseGame)
    : Scene(window),
      m_fnPauseGame(fnPauseGame)
{
    test();
}

void GameScene::update(std::chrono::duration<float> elapsed)
{
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        m_fnPauseGame();

    for (auto &system : m_systems)
        system->preUpdate(m_registry, elapsed);

    for (auto &system : m_systems)
        system->update(m_registry, elapsed);

    for (auto &system : m_systems)
        system->postUpdate(m_registry, elapsed);
}

Level::LoadResult GameScene::loadLevel(const std::string &levelPath)
{
    std::cout << __FUNCTION__ << " " << levelPath << "\n";

    m_systems.emplace_back(std::make_unique<System::Controls>(m_window, m_level));
    m_systems.emplace_back(std::make_unique<System::Movement>(m_level));
    m_systems.emplace_back(std::make_unique<System::Graphics>(m_window, m_level));
    m_systems.emplace_back(std::make_unique<System::Debug>(m_window));


    //m_level.loadFromDisk("levels/test_level_1");
    //m_level.loadFromFile("deb1block.lvl");
    //m_level.loadIt();

    Level::LoadResult loadResult = m_level.loadFromDisk(levelPath);

    if (!loadResult.success)
    {
        std::cerr << "failed to load level " << levelPath << "\n";
        return loadResult;
    }

    {
        entt::entity entityPlayer = m_registry.create();
        m_registry.emplace<Component::Position>(entityPlayer, m_level.playerStartPos().x, m_level.playerStartPos().y, m_level.playerStartAngle());
        m_registry.emplace<Component::Player>(entityPlayer);
        m_registry.emplace<Component::MoveStepwise>(entityPlayer, 0.0f, 0.0f, 0.0f);
    }

    return Level::LoadResult::makeOk();

    //    {   // lamp 1
    //        entt::entity entityLamp = m_registry.create();
    //        m_registry.emplace<Component::Position>(entityLamp, 8.5f, 8.5f, 0.0f);
    //        m_registry.emplace<Component::RemoveFlag>(entityLamp);
    //        m_registry.emplace<Component::Entity>(entityLamp);
    //    }
    //    {   // lamp 2
    //        entt::entity entityLamp = m_registry.create();
    //        m_registry.emplace<Component::Position>(entityLamp, 7.5f, 7.5f, 0.0f);
    //        m_registry.emplace<Component::RemoveFlag>(entityLamp);
    //        m_registry.emplace<Component::Entity>(entityLamp);
    //    }
    //    {   // lamp 3
    //        entt::entity entityLamp = m_registry.create();
    //        m_registry.emplace<Component::Position>(entityLamp, 10.5f, 3.5f, 0.0f);
    //        m_registry.emplace<Component::RemoveFlag>(entityLamp);
    //        m_registry.emplace<Component::Entity>(entityLamp);
    //    }
}
