#include "quickmath.h"

#include "quickrand.h"

#include <fmt/format.h>

namespace QuickMath
{

sf::Vector2f max(const sf::Vector2f &vec1, const sf::Vector2f &vec2)
{
    return { std::max(vec1.x, vec2.x), std::max(vec1.y, vec2.y) };
}

/** Calculate determinant of matrix:
    [a b]
    [c d]
*/
extern inline float det(float a, float b, float c, float d)
{
    return a * d - b * c;
}

/// Calculate intersection of two lines.
/// \return true if found, false if not found or error
bool lineLineIntersect(sf::Vector2f v1Start, sf::Vector2f v1End, sf::Vector2f v2Start, sf::Vector2f v2End, sf::Vector2f &vOut)
{
    float detL1 = det(v1Start.x, v1Start.y, v1End.x, v1End.y);
    float detL2 = det(v2Start.x, v2Start.y, v2End.x, v2End.y);
    float x1mx2 = v1Start.x - v1End.x;
    float x3mx4 = v2Start.x - v2End.x;
    float y1my2 = v1Start.y - v1End.y;
    float y3my4 = v2Start.y - v2End.y;

    float xnom = det(detL1, x1mx2, detL2, x3mx4);
    float ynom = det(detL1, y1my2, detL2, y3my4);
    float denom = det(x1mx2, y1my2, x3mx4, y3my4);

    if(denom == 0.0F)   //Lines don't seem to cross
    {
        vOut.x = NAN;
        vOut.y = NAN;
        return false;
    }

    vOut.x = xnom / denom;
    vOut.y = ynom / denom;

    if(!std::isfinite(vOut.x) || !std::isfinite(vOut.y))    //Probably a numerical issue
        return false;

    if ((vOut.x > v1End.x || vOut.x < v1Start.x || vOut.y > v1End.y || vOut.y < v1Start.y))
        return false;

    return true;
}


// Given three colinear points p, q, r, the function checks if
// point q lies on line segment 'pr'
bool onSegment(sf::Vector2f p, sf::Vector2f q, sf::Vector2f r)
{
    return (q.x <= std::max(p.x, r.x) && q.x >= std::min(p.x, r.x) &&
            q.y <= std::max(p.y, r.y) && q.y >= std::min(p.y, r.y));
}

// To find orientation of ordered triplet (p, q, r).
// The function returns following values
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// 2 --> Counterclockwise
int orientation(sf::Vector2f p, sf::Vector2f q, sf::Vector2f r)
{
    // See https://www.geeksforgeeks.org/orientation-3-ordered-points/
    // for details of below formula.
    float val = (q.y - p.y) * (r.x - q.x) -
                (q.x - p.x) * (r.y - q.y);

    return (val > 0)? 1: 2; // clock or counterclock wise
}


// The main function that returns true if line segment 'p1q1'
// and 'p2q2' intersect.
bool doIntersect(sf::Vector2f p1, sf::Vector2f q1, sf::Vector2f p2, sf::Vector2f q2)
{
    // Find the four orientations needed for general and
    // special cases
    int o1 = orientation(p1, q1, p2);
    int o2 = orientation(p1, q1, q2);
    int o3 = orientation(p2, q2, p1);
    int o4 = orientation(p2, q2, q1);

    // General case
    if (o1 != o2 && o3 != o4)
        return true;

    // Special Cases
    // p1, q1 and p2 are colinear and p2 lies on segment p1q1
    if (o1 == 0 && onSegment(p1, p2, q1)) return true;

    // p1, q1 and q2 are colinear and q2 lies on segment p1q1
    if (o2 == 0 && onSegment(p1, q2, q1)) return true;

    // p2, q2 and p1 are colinear and p1 lies on segment p2q2
    if (o3 == 0 && onSegment(p2, p1, q2)) return true;

     // p2, q2 and q1 are colinear and q1 lies on segment p2q2
    if (o4 == 0 && onSegment(p2, q1, q2)) return true;

    return false; // Doesn't fall in any of the above cases
}

sf::Vector2f getUnitVector(sf::Vector2f vec)
{
    // Get vector magnitude
    float m = std::sqrt(vec.x * vec.x + vec.y * vec.y);

    // Divide by magnitude
    vec.x /=  m;
    vec.y /=  m;

    return vec;
}

float getDistance(const sf::Vector2f &p1, const sf::Vector2f &p2)
{
    return static_cast<float>(std::sqrt(std::pow(p2.x - p1.x, 2) + std::pow(p2.y - p1.y, 2)));
}

sf::Vector2f rotate(const sf::Vector2f &vec, float angle, const sf::Vector2f &vecAround)
{
    float angleRad = angle * M_PIf32 / 180;
    float sinAngleRad = std::sin(angleRad);
    float cosAngleRad = std::cos(angleRad);
    sf::Vector2f vecDiff = vec - vecAround;

    return sf::Vector2f(cosAngleRad * vecDiff.x - sinAngleRad * vecDiff.y + vecAround.x,
                        sinAngleRad * vecDiff.x + cosAngleRad * vecDiff.y + vecAround.y);
}

sf::Vector2f rotateRandom(const sf::Vector2f &vec, float floor, float ceil)
{
    return QuickMath::rotate(vec, QuickRand::instance().get<float>(floor, ceil));
}

float vectorLength(const sf::Vector2f &vec)
{
    return std::sqrt(vec.x * vec.x + vec.y * vec.y);
}

float vectorLengthSquared(const sf::Vector2f &vec)
{
    return (vec.x * vec.x + vec.y * vec.y);
}

sf::Vector2f lineLineIntersection(const sf::Vector2f &vecA, const sf::Vector2f &vecB, const sf::Vector2f &vecC, const sf::Vector2f &vecD)
{
    // Line AB represented as a1x + b1y = c1
    float a1 = vecB.y - vecA.y;
    float b1 = vecA.x - vecB.x;
    float c1 = a1*(vecA.x) + b1*(vecA.y);

    // Line CD represented as a2x + b2y = c2
    float a2 = vecD.y - vecC.y;
    float b2 = vecC.x - vecD.x;
    float c2 = a2*(vecC.x)+ b2*(vecC.y);

    float determinant = a1*b2 - a2*b1;

    float x = (b2*c1 - b1*c2)/determinant;
    float y = (a1*c2 - a2*c1)/determinant;
    return sf::Vector2f(x, y);
}

float angleOfLineDeg(sf::Vector2f p0, sf::Vector2f p1)
{
    float angle = atan2f(p0.y - p1.y, p0.x - p1.x);

    angle = angle * 180.0F / M_PIf32;

    angle -= 90;
    if (angle < 0) angle += 360;

    return angle;
}

float angleOfLine(sf::Vector2f p0, sf::Vector2f p1)
{
    float angle = ::atan2f(p0.y - p1.y, -p0.x - -p1.x);

    if (angle < 0)
        angle += QuickMath::pi * 2.0f;

    return angle;
}

sf::Vector2f moveAngular(sf::Vector2f pos, float rot, float distance)
{
    pos.y -= distance;
    float deg = rot * 180 / QuickMath::pi;
    return rotate(pos, deg);

//    float actualRot = rot - 90;
//    if (actualRot < 0)
//        actualRot = 360 + actualRot;

//    return { static_cast<float>(pos.x + std::cos(actualRot * QuickMath::pi / 180.0) * distance),
    //             static_cast<float>(pos.y + std::sin(actualRot * QuickMath::pi / 180.0) * distance) };
}

float angleDiff(float a1, float a2)
{
//    float diff = a2 - a1;
//    return std::fmod(diff, QuickMath::pi);
    double difference = a2 - a1;
    while (difference < -QuickMath::pi)

        difference += QuickMath::pi * 2.0f;
    while (difference > QuickMath::pi)

        difference -= QuickMath::pi * 2.0f;
    return difference;
}




} // namespace QM
