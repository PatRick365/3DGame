#pragma once

#include <iostream>
#include <cmath>
#include <random>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>

namespace QuickMath
{

static constexpr float pi = M_PI;

extern sf::Vector2f max(const sf::Vector2f &vec1, const sf::Vector2f &vec2);

extern sf::Vector2f getUnitVector(sf::Vector2f vec);

extern float getDistance(const sf::Vector2f &p1, const sf::Vector2f &p2);

extern bool doIntersect(sf::Vector2f p1, sf::Vector2f q1, sf::Vector2f p2, sf::Vector2f q2);
extern bool lineLineIntersect(sf::Vector2f v1Start, //Line 1 start
                              sf::Vector2f v1End, //Line 1 end
                              sf::Vector2f v2Start, //Line 2 start
                              sf::Vector2f v2End, //Line 2 end
                              sf::Vector2f &vOut); //Output

extern sf::Vector2f rotate(const sf::Vector2f &vec, float angle, const sf::Vector2f &vecAround = sf::Vector2f());
extern sf::Vector2f rotateRandom(const sf::Vector2f &vec, float floor, float ceil);

extern float vectorLength(const sf::Vector2f &vec);
extern float vectorLengthSquared(const sf::Vector2f &vec);
extern sf::Vector2f lineLineIntersection(const sf::Vector2f &vecA, const sf::Vector2f &vecB, const sf::Vector2f &vecC, const sf::Vector2f &vecD);

extern float angleOfLineDeg(sf::Vector2f p0, sf::Vector2f p1);
extern float angleOfLine(sf::Vector2f p0, sf::Vector2f p1);
extern float angleDiff(float a1, float a2);

extern sf::Vector2f moveAngular(sf::Vector2f pos, float rot, float distance);

} // namespace QM
