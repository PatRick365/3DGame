#pragma once

#include <string>

#include <SFML/Graphics.hpp>


class Material
{
public:
    Material() = default;
    explicit Material(const std::string &filePath);

    const sf::Image &image() const;

    void applyTaint(const sf::Color &color);

    bool isTransparent() const;

    std::string filePath() const;

    sf::Color taint() const;

private:
    std::string m_filePath;
    sf::Color m_taint;
    sf::Image m_image;
    bool m_isTransparent = false;
};

