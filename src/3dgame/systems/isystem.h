#pragma once

#include "entt/entt.hpp"

#include <chrono>

namespace System
{

class ISystem
{
public:
    virtual ~ISystem() = default;

    virtual void preUpdate(entt::registry &/*registry*/, std::chrono::duration<float> /*elapsed*/) {}
    virtual void update(entt::registry &/*registry*/, std::chrono::duration<float> /*elapsed*/) {}
    virtual void postUpdate(entt::registry &/*registry*/, std::chrono::duration<float> /*elapsed*/) {}
};

}

