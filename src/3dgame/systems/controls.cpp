#include "controls.h"

#include <cassert>
#include <cmath>

#include "../components.h"


namespace System
{

static constexpr float fSpeed = 5.0f;			// Walking Speed

Controls::Controls(sf::RenderWindow &window, Level &level)
    : m_window(window),
      m_level(level)
{}

void Controls::preUpdate(entt::registry &registry, std::chrono::duration<float> elapsed)
{
    sf::Event event;
    while (m_window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
            m_window.close();
    }

    auto view = registry.view<Component::Player, Component::MoveStepwise, Component::Position>();
    assert(!view.empty());

    entt::entity entityPlayer = view.front();

    auto &playerMoveStepwise = view.get<Component::MoveStepwise>(entityPlayer);
    auto &playerMovePosition = view.get<Component::Position>(entityPlayer);

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) ^ sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            playerMoveStepwise.dangle -= (fSpeed * 0.5f);
        }
        else
        {
            playerMoveStepwise.dangle += (fSpeed * 0.5f);
        }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) ^ sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            playerMoveStepwise.dx -= ::sinf(playerMovePosition.angle) * fSpeed;
            playerMoveStepwise.dy -= ::cosf(playerMovePosition.angle) * fSpeed;
        }
        else
        {
            playerMoveStepwise.dx += ::sinf(playerMovePosition.angle) * fSpeed;
            playerMoveStepwise.dy += ::cosf(playerMovePosition.angle) * fSpeed;
        }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Y) ^ sf::Keyboard::isKeyPressed(sf::Keyboard::X))
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Y))
        {
            playerMoveStepwise.dx -= ::cosf(playerMovePosition.angle) * (fSpeed * 0.5f);
            playerMoveStepwise.dy += ::sinf(playerMovePosition.angle) * (fSpeed * 0.5f);
        }
        else
        {
            playerMoveStepwise.dx += ::cosf(playerMovePosition.angle) * (fSpeed * 0.5f);
            playerMoveStepwise.dy -= ::sinf(playerMovePosition.angle) * (fSpeed * 0.5f);
        }
    }

}

}
