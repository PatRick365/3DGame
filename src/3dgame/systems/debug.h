#pragma once

#include <chrono>
#include <string>

#include "isystem.h"
#include "../level.h"


namespace System
{

using TimeStamp = std::chrono::time_point<std::chrono::system_clock, std::chrono::microseconds>;

class Debug : public ISystem
{
public:
    explicit Debug(sf::RenderWindow &window);

    void update(entt::registry &registry, std::chrono::duration<float> elapsed) override;

private:
    std::string m_strPos;
    std::string m_strFps;

    sf::RenderWindow &m_window;
    sf::Font m_font;
    TimeStamp m_timestamp;
    size_t m_frameCount = 0;
    sf::Text m_textFps;

};

}
