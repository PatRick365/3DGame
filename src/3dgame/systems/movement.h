#pragma once

#include "isystem.h"
#include "../level.h"

namespace System
{

class Movement : public ISystem
{
public:
    Movement(Level &level);

    void update(entt::registry &registry, std::chrono::duration<float> elapsed) override;

private:
    Level &m_level;
};

}
