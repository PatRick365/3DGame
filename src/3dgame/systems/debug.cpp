#include "debug.h"

#include <algorithm>
#include <numeric>
#include <sstream>
#include <iomanip>

#define FMT_HEADER_ONLY
#include "../extern/fmt/format.h"

#include "components.h"


namespace System
{

using namespace std::chrono;
using namespace std::chrono_literals;


Debug::Debug(sf::RenderWindow &window)
    : m_window(window)
{
    m_font.loadFromFile("resources/UbuntuMono-B.ttf");

    m_textFps = sf::Text("/", m_font);

    m_timestamp = std::chrono::time_point_cast<microseconds>(system_clock::now());

    m_textFps.setPosition(5, 2);
    //m_textFps.setLineSpacing(0.5);
}

void Debug::update(entt::registry &registry, std::chrono::duration<float> /*elapsed*/)
{
    ++m_frameCount;

    auto view = registry.view<Component::Position, Component::Player>();
    assert(!view.empty());
    entt::entity entityPlayer = view.front();
    auto &playerPos = view.get<Component::Position>(entityPlayer);

//    std::stringstream ss;
//    //ss.precision(2);
//    ss << std::setprecision(2);
//    ss << "POS(" << playerPos.x << "," << playerPos.y << ") a:" << playerPos.angle << "\n";
//    m_strPos = ss.str();

    m_strPos = fmt::format("POS({:.{}f},{:.{}f}) A: {:.{}f}", playerPos.x, 2, playerPos.y, 2, playerPos.angle, 2);

    if ((std::chrono::time_point_cast<microseconds>(system_clock::now()) - m_timestamp) >= 1s)
    {
        m_strFps = "FPS: " + std::to_string(m_frameCount);
        m_frameCount = 0;
        m_timestamp = std::chrono::time_point_cast<microseconds>(system_clock::now());
    }

    m_textFps.setString(m_strPos + "\n" + m_strFps);

    m_window.setView(sf::View(sf::Vector2f(m_window.getSize().x / 2, m_window.getSize().y / 2), sf::Vector2f(m_window.getSize().x, m_window.getSize().y)));

    m_window.draw(m_textFps);
}

}
