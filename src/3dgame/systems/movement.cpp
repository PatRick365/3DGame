#include "movement.h"

#include <cmath>
#include <iostream>

#include "../components.h"
#include "raycaster.h"


namespace System
{

Movement::Movement(Level &level)
    : m_level(level)
{}

void Movement::update(entt::registry &registry, std::chrono::duration<float> elapsed)
{
    {
        auto view = registry.view<Component::Player, Component::MoveStepwise, Component::Position>();
        assert(!view.empty());

        entt::entity entityPlayer = view.front();

        auto &playerMoveStepwise = view.get<Component::MoveStepwise>(entityPlayer);
        auto &playerPosition = view.get<Component::Position>(entityPlayer);

        playerPosition.angle += playerMoveStepwise.dangle * elapsed.count();

        if (playerPosition.angle < 0)
            playerPosition.angle += 2 * QuickMath::pi;

        playerPosition.angle = ::fmodf(playerPosition.angle, 2 * QuickMath::pi);

        sf::Vector2f vecPlayer(playerPosition.x, playerPosition.y);
        sf::Vector2f vecTarget(playerPosition.x + playerMoveStepwise.dx * elapsed.count(), playerPosition.y + playerMoveStepwise.dy * elapsed.count());
        playerMoveStepwise.reset();

        {
            RayCaster rayCaster(m_level.m_geometries);

            if (std::optional<sf::Vector2f> vecIntersection = rayCaster.calcClosestIntersection(vecPlayer, vecTarget, 0.1))
            {
                //static size_t iii = 0;
                //std::cout << ++iii << " yes\n";

                 sf::Vector2f vecMoveUnit = QuickMath::getUnitVector(vecIntersection.value() - vecPlayer);
                 playerPosition.x = vecIntersection->x - (vecMoveUnit.x / 100.0f);
                 playerPosition.y = vecIntersection->y - (vecMoveUnit.y / 100.0f);

//                printf("rayAngle: %.2f, vecUnit(%.2f,%.2f), pos(%.2f,%.2f), target(%.2f,%.2f), intersection(%.2f,%.2f)\n", rayAngle,
//                       vecUnit.x, vecUnit.y,
//                       vecPlayer.x, vecPlayer.y,
//                       vecTarget.x, vecTarget.y,
//                       intersection->x, intersection->y);
            }
            else
            {
                //std::cout << "no\n";
                playerPosition.x = vecTarget.x;
                playerPosition.y = vecTarget.y;
            }
        }


//        if (!m_level.m_bounds.contains(playerPosition.x, playerPosition.y))
//        {
//            playerPosition.x = playerPositionBefore.x;
//            playerPosition.y = playerPositionBefore.y;
//        }

        //if (m_level.map().c_str()[(int)playerPosition.x * m_level.w() + (int)playerPosition.y] == '#')
        //if (m_level.map()[static_cast<int>(playerPosition.x) * m_level.w() + static_cast<int>(playerPosition.y)].isSolid())
//        if (m_level.mapTile(static_cast<int>(playerPosition.x), static_cast<int>(playerPosition.y)).isSolid())
//        {
//            playerPosition.x = playerPositionBefore.x;
//            playerPosition.y = playerPositionBefore.y;
//        }
    }

    // gets all the components of the view at once ...
//    registry.view<Component::Position, Component::Velocity>().each([elapsed](auto &pos, auto &vel)
//    {
//        pos.x += vel.dx * elapsed.count();
//        pos.y += vel.dy * elapsed.count();
//    });
}

}
