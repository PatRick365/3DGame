#pragma once

#include <SFML/Graphics.hpp>

#include "isystem.h"
#include "../level.h"


namespace System
{

class Controls : public ISystem
{
public:
    Controls(sf::RenderWindow &window, Level &level);

    void preUpdate(entt::registry &registry, std::chrono::duration<float> elapsed) override;

private:
    sf::RenderWindow &m_window;
    Level &m_level;
};

}
