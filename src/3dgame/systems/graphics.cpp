#include "graphics.h"

#include <cmath>
#include <iostream>

#include "components.h"
#include "quickmath.h"
#include "raycaster.h"


static constexpr float fFOV = 3.14159f / 4.0f;	// Field of View
static constexpr float fDepth = 64.0f;			// Maximum rendering distance

//std::ostream &operator<<(std::ostream &os, const sf::Color &col)
//{
//    return os << "sf::Color(" << int(col.r) << ", " << int(col.g) << ", " << int(col.b) << ")";
//}

namespace System
{

sf::Color operator*(const sf::Color &lhs, float rhs)
{
    return sf::Color(lhs.r * rhs, lhs.g * rhs, lhs.b * rhs);
}

Graphics::Graphics(sf::RenderWindow &window, Level &level)
    : m_window(window),
      m_level(level)
{
    //    m_texturesEntities[0].loadFromFile("lamp.png");
}

void Graphics::preUpdate(entt::registry &/*registry*/, std::chrono::duration<float> /*elapsed*/)
{
    m_window.clear();
}

void Graphics::update(entt::registry &registry, std::chrono::duration<float> /*elapsed*/)
{
    m_window.setView(sf::View(sf::Vector2f(nScreenWidth / 2, nScreenHeight / 2), sf::Vector2f(nScreenWidth, nScreenHeight)));

    RayCaster rayCaster(m_level.m_geometries);

    auto view = registry.view<Component::Position, Component::Player>();
    assert(!view.empty());

    entt::entity entityPlayer = view.front();

    auto &playerPos = view.get<Component::Position>(entityPlayer);


    for (int pixelX = 0; pixelX < nScreenWidth; pixelX++)
    {
        // For each column, calculate the projected ray angle into world space
        float fRayAngle = (playerPos.angle - fFOV / 2.0f) + ((float)pixelX / (float)nScreenWidth) * fFOV;

        sf::Vector2f rayBegin(playerPos.x, playerPos.y);
        float fEyeX = ::sinf(fRayAngle); // Unit vector for ray in player space
        float fEyeY = ::cosf(fRayAngle);
        sf::Vector2f rayEnd = sf::Vector2f((playerPos.x + fEyeX * fDepth), (playerPos.y + fEyeY * fDepth));

        std::vector<RayCaster::Collision> collisions = rayCaster.calcIntersections(rayBegin, rayEnd);

        bool firstRun = true;

        if (collisions.empty() || collisions.front().geo->isTransparent())
        {
            drawColumnLayer(pixelX, nullptr, 0, 0, firstRun);
        }

        //std::cout << "collisions: " << collisions.size() << "\n";

        for (const RayCaster::Collision &collision : collisions)
        {
            Geometry::MaterialId materialId = collision.geo->materialId();
            Material &material = m_level.wallTexture(materialId);

            //std::cout << "mat: " << size_t(collision.blockSide) << "\n";

            drawColumnLayer(pixelX, &material, collision.distance, collision.fSampleX, firstRun);
        }
    }


    auto viewObjects = registry.view<Component::Position, Component::Entity>();
//    std::sort(viewObjects.begin(), viewObjects.end(), [](const auto &lhs, const auto &rhs)
//    {

//    });

//    registry.sort<Component::Entity>([](const auto &lhs, const auto &rhs)
//    {
//        return lhs.z < rhs.z;
//    });


    for (entt::entity entity : viewObjects)
    {
        Component::Position objPos = viewObjects.get<Component::Position>(entity);
        sf::Image imageEntity = m_texturesEntities[0];
        //sf::RectangleShape rect(sf::Vector2f(pixelWidth, pixelHeight));

        // Update Object Physics
//        object.x += object.vx * fElapsedTime;
//        object.y += object.vy * fElapsedTime;

        // Check if object is inside wall - set flag for removal
//        if (map.c_str()[(int)object.x * m_level.w() + (int)object.y] == '#')
//            object.bRemove = true;

        // Can object be seen?
        float fVecX = objPos.x - playerPos.x;
        float fVecY = objPos.y - playerPos.y;
        float fDistanceFromPlayer = sqrtf(fVecX*fVecX + fVecY*fVecY);

        float fEyeX = sinf(playerPos.angle);
        float fEyeY = cosf(playerPos.angle);

        // Calculate angle between lamp and players feet, and players looking direction
        // to determine if the lamp is in the players field of view
        float fObjectAngle = atan2f(fEyeY, fEyeX) - atan2f(fVecY, fVecX);
        if (fObjectAngle < -3.14159f)
            fObjectAngle += 2.0f * 3.14159f;
        if (fObjectAngle > 3.14159f)
            fObjectAngle -= 2.0f * 3.14159f;

        bool bInPlayerFOV = fabs(fObjectAngle) < fFOV / 2.0f;

        if (bInPlayerFOV && fDistanceFromPlayer >= 0.5f && fDistanceFromPlayer < fDepth /*&& !object.bRemove*/)
        {
            float fObjectCeiling = (float)(nScreenHeight / 2.0) - nScreenHeight / ((float)fDistanceFromPlayer);
            float fObjectFloor = nScreenHeight - fObjectCeiling;
            float fObjectHeight = fObjectFloor - fObjectCeiling;
            float fObjectAspectRatio = (float)imageEntity.getSize().y / (float)imageEntity.getSize().x;
            float fObjectWidth = fObjectHeight / fObjectAspectRatio;
            float fMiddleOfObject = (0.5f * (fObjectAngle / (fFOV / 2.0f)) + 0.5f) * (float)nScreenWidth;

            // Draw Lamp
            for (float lx = 0; lx < fObjectWidth; lx++)
            {
                for (float ly = 0; ly < fObjectHeight; ly++)
                {
                    float fSampleX = lx / fObjectWidth;
                    float fSampleY = ly / fObjectHeight;

                    unsigned int ix = imageEntity.getSize().x * fSampleX;
                    unsigned int iy = imageEntity.getSize().y * fSampleY;

                    sf::Color c = imageEntity.getPixel(ix, iy);

                    if (int(c.a) == 0)
                    {
                        continue;
                    }

                    int nObjectColumn = (int)(fMiddleOfObject + lx - (fObjectWidth / 2.0f));
                    if (nObjectColumn >= 0 && nObjectColumn < nScreenWidth)
                    {
                        if (m_depthBuffer[nObjectColumn] >= fDistanceFromPlayer)
                        {
                            m_depthBuffer[nObjectColumn] = fDistanceFromPlayer;

                            //m_pixelBuffer.setPixel(nObjectColumn * pixelWidth, (fObjectCeiling + ly) * pixelHeight, c);
                            //m_pixelBuffer.setPixel(nObjectColumn * 1, (fObjectCeiling + ly) * 1, c);
                        }
                    }
                }
            }
        }
    }

    m_window.draw(m_pixelBuffer);


    // Remove dead objects from object list
//    listObjects.remove_if([](sObject &o) {return o.bRemove; });

//    // Display Map & Player
//    for (int nx = 0; nx < m_level.w(); nx++)
//        for (int ny = 0; ny < m_level.w(); ny++)
//            Draw(nx+1, ny+1, map[ny * m_level.w() + nx]);
//    Draw(1 + (int)fPlayerY, 1 + (int)fPlayerX, L'P');

//    sf::Font font;
//    font.loadFromFile("computo-monospace.otf");
//    sf::Text text(debugString, font);
//    m_window.draw(text);

}

void Graphics::postUpdate(entt::registry &/*registry*/, std::chrono::duration<float> /*elapsed*/)
{
    m_window.display();
}

void Graphics::drawColumnLayer(int pixelX, Material *material, float distanceToWall, float fSampleX, bool &firstRun)
{
    // Update Depth Buffer
    m_depthBuffer[pixelX] = distanceToWall;

    // Calculate distance to ceiling and floor
    int nCeiling = (nScreenHeight / 2.0f) - nScreenHeight / distanceToWall;
    int nFloor = nScreenHeight - nCeiling;

    if (material == nullptr)
    {
        nCeiling = (nScreenHeight / 2.0);
        nFloor = nScreenHeight - nCeiling;
    }

    //std::cout << "distanceToWall: " << distanceToWall << " nCeiling: " << nCeiling << " nFloor: " << nFloor << "\n";

    for (int pixelY = 0; pixelY < nScreenHeight; pixelY++)
    {
        // Draw Ceiling
        if (pixelY <= nCeiling)
        {
            float b = 1.0f - (pixelY - nScreenHeight / 4.0f) / (nScreenHeight / 4.0f);
            sf::Color nShade = sf::Color::Black + (m_level.colorCeiling() * b);
            m_pixelBuffer.setPixel(pixelX, pixelY, nShade);

//            float a = nScreenHeight / static_cast<float>(pixelY);
//            sf::Color nShade = sf::Color::Black + (m_level.colorCeiling() * (1 - a));
//            m_pixelBuffer.setPixel(pixelX, pixelY, nShade);
//            m_pixelBuffer.setPixel(pixelX, pixelY, m_level.colorCeiling());
        }
        else if (pixelY > nCeiling && pixelY <= nFloor)
        {
            // Draw Wall
            if (distanceToWall < fDepth)
            {
                assert(material);
                assert(material->image().getSize().x == 32);
                assert(material->image().getSize().y == 32);

                float fSampleY = ((float)pixelY - (float)nCeiling) / ((float)nFloor - (float)nCeiling);

                unsigned int imgX = material->image().getSize().x - 1;
                unsigned int imgY = material->image().getSize().y - 1;

//                unsigned int ix = static_cast<unsigned int>((imgX * fSampleX + 0.5f)) % (imgX);
//                unsigned int iy = static_cast<unsigned int>((imgY * fSampleY + 0.5f)) % (imgY);
                unsigned int ix = static_cast<unsigned int>((imgX * (fSampleX * 4.0f) + 0.5f)) % (imgX);
                unsigned int iy = static_cast<unsigned int>((imgY * (fSampleY * 4.0f) + 0.5f)) % (imgY);

                sf::Color col = material->image().getPixel(ix, iy);
                float factor = (distanceToWall / fDepth) * 2.5f;
                factor = std::min(factor, 1.0f);
                col.r -= col.r * factor;
                col.g -= col.g * factor;
                col.b -= col.b * factor;

                float alpha = 1.0f - (col.a / 255.0f);

                if (!firstRun && (alpha > 0))
                {
                    m_pixelBuffer.combine(pixelX, pixelY, col);
                }
                else
                {
                    m_pixelBuffer.setPixel(pixelX, pixelY, col);
                }
            }
            else
            {
                m_pixelBuffer.setPixel(pixelX, pixelY, sf::Color::Black);
            }
        }
        else // Floor
        {
            float b = 1.0f - (pixelY - nScreenHeight / 2.0f) / (nScreenHeight / 2.0f);
            sf::Color nShade = sf::Color::Black + (m_level.colorFloor() * (1 - b));
            m_pixelBuffer.setPixel(pixelX, pixelY, nShade);
        }
    }

    firstRun = false;
}


}
