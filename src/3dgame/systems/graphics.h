#pragma once

#include <unordered_map>

#include <SFML/Graphics.hpp>

#include "isystem.h"
#include "../level.h"


static constexpr int resX = 1200;
static constexpr int resY = 800;

static constexpr int nScreenHeight = resY / 2;			// Console Screen Size Y (rows)
static constexpr int nScreenWidth = resX / 2;			// Console Screen Size X (columns)

template<int W, int H>
struct PixelBuffer :  public sf::Drawable
{
    PixelBuffer()
    {
        m_data.fill(static_cast<sf::Int8>(255));
    }

    void setPixel(size_t x, size_t y, const sf::Color &color)
    {
        size_t i = (x + (y * W)) * 4;
        assert(i < m_data.size());

        m_data[i + 0] = color.r;
        m_data[i + 1] = color.g;
        m_data[i + 2] = color.b;
        //m_data[i + 3] = color.a;
    }

    sf::Color getPixel(size_t x, size_t y)
    {
        size_t i = (x + (y * W)) * 4;

        assert(i < m_data.size());

        return sf::Color(m_data[i + 0], m_data[i + 1], m_data[i + 2], m_data[i + 3]);
    }

    void combine(size_t x, size_t y, const sf::Color &color)
    {
        size_t i = (x + (y * W)) * 4;
        assert(i < m_data.size());

        float alpha = 1.0f - (color.a / 255.0f);

        sf::Color oldCol(m_data[i + 0], m_data[i + 1], m_data[i + 2], m_data[i + 3]);
        m_data[i + 0] = (oldCol.r * alpha) + (color.r * (1.0 - alpha));
        m_data[i + 1] = (oldCol.g * alpha) + (color.g * (1.0 - alpha));
        m_data[i + 2] = (oldCol.b * alpha) + (color.b * (1.0 - alpha));
    }

protected:
    void draw(sf::RenderTarget &target, sf::RenderStates /*states*/) const override
    {
        sf::Texture bufferTexture;
        bufferTexture.create(nScreenWidth, nScreenHeight);
        bufferTexture.update(m_data.data());
        sf::Sprite spriteBuffer(bufferTexture);
        target.draw(spriteBuffer);
    }

private:
    std::array<sf::Uint8, W * H * 4> m_data;
};

namespace System
{

class Graphics : public ISystem
{
public:
    Graphics(sf::RenderWindow &window, Level &level);

    void preUpdate(entt::registry &registry, std::chrono::duration<float> elapsed) override;
    void update(entt::registry &registry, std::chrono::duration<float> elapsed) override;
    void postUpdate(entt::registry &registry, std::chrono::duration<float> elapsed) override;

    void drawColumnLayer(int pixelX, Material *material, float distanceToWall, float fSampleX, bool &firstRun);

private:
    sf::RenderWindow &m_window;
    Level &m_level;
    PixelBuffer<nScreenWidth, nScreenHeight> m_pixelBuffer;
    std::unordered_map<int, sf::Image> m_texturesEntities;
    std::array<float, nScreenWidth> m_depthBuffer;

};

}
